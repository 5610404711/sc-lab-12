package Four;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Set;

import Two.Homework;

public class WordCounter {
	private String filename;
	private HashMap<String,Integer> wordCount;
	
	public WordCounter(String fliename){
		this.filename = fliename;
		wordCount = new HashMap<String,Integer>();
	}

	public void count(){
		
		FileReader fileReader = null;
		try {
			fileReader = new FileReader(filename);
			BufferedReader buffer = new BufferedReader(fileReader);
			String line;
			for (line = buffer.readLine(); line != null; line = buffer.readLine()){
				String[] data = line.split(" ");
				for (String str:data){
					
					if(wordCount.containsKey(str)){
						wordCount.put(str, wordCount.get(str)+1);
					}else{
						wordCount.put(str, 1);
					}
				}
			}
		}	
			
		catch (FileNotFoundException e){
			System.err.println("Cannot read file "+filename);
		}
		catch (IOException e){
			System.err.println("Error reading from file");
		}
		finally {
			try {
				if (fileReader != null)
						fileReader.close();
			} catch (IOException e) {
				System.err.println("Error closing files");
			}
		}
		
	}
	
	public int hasWord(String word){
		if(wordCount.containsKey(word)){
			return wordCount.get(word);
		}else{
			return 0;
		}
		
	}
	
	public String getCountData(){
		String st = "";
		Set<String> keys = wordCount.keySet();
		for (String name : keys) {
		Integer num = wordCount.get(name);
		System.out.println(name+"="+num);
		st = st+name+"="+num+"\n";
		}
		return st;
	}


}
