package One;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class PhoneBook {
	private String name;
	private String number;
	
	public PhoneBook(String name, String number){
		this.name = name;
		this.number = number;	
	}
	
	public String toString(){
		return name+"   "+number;
	}

}
