package One;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class PhoneBookMain {
	
	public static void main(String[] args) {
		String filename = "PhoneBook.txt";
		ArrayList<PhoneBook> plist = new ArrayList<PhoneBook>();

		try {
			FileReader fileReader = new FileReader(filename);
			BufferedReader buffer = new BufferedReader(fileReader);
			String line;
			for (line = buffer.readLine(); line != null; line = buffer.readLine()){
				String[] data = line.split(", ");
				String name = data[0].trim();
				String number = data[1].trim();
				
				PhoneBook phone = new PhoneBook(name,number);
				plist.add(phone);		
			}
			System.out.println("------- Phone Book -------");
			System.out.println(" ");
			System.out.println("Name       Phone");
			System.out.println("-----  -------------");
			
			for(PhoneBook p: plist){
				System.out.println(p.toString());
			}
			
			System.out.println("---------------------");
			
			
		}
		catch (FileNotFoundException e){
			System.err.println("Cannot read file "+filename);
			}
		catch (IOException e){
			System.err.println("Error reading from file");
			}

	}

}
