package Two;

public class Homework {
	private String name;
	private double score;

	public Homework(String name){
		this.name = name;
		this.score = 0;
	}
	
	public void setScore(double score){
		this.score += score;
	}
	
	public double getScore(){
		return score;
	}
	
	public String getName(){
		return name;
	}
	
}
