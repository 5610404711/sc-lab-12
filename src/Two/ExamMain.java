package Two;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class ExamMain {

	public static void main(String[] args) {
		String filename = "Homework.txt";
		ArrayList<Homework> hlist = new ArrayList<Homework>();
		ArrayList<Homework> elist = new ArrayList<Homework>();
		FileReader fileReader = null;
		try {
			fileReader = new FileReader(filename);
			BufferedReader buffer = new BufferedReader(fileReader);
			String line;
			for (line = buffer.readLine(); line != null; line = buffer.readLine()){
				String[] data = line.split(", ");
				String name = data[0].trim();
				Double s1 = Double.parseDouble(data[1].trim());
				Double s2 = Double.parseDouble(data[2].trim());
				Double s3 = Double.parseDouble(data[3].trim());
				Double s4 = Double.parseDouble(data[4].trim());
				Double s5 = Double.parseDouble(data[5].trim());
				double sum = s1+s2+s3+s4+s5;
				
				Homework hw = new Homework(name);
				hw.setScore(sum);
				hlist.add(hw);
				
			}
			
			filename = "exam.txt";
			fileReader = new FileReader(filename);
			buffer = new BufferedReader(fileReader);
			for (line = buffer.readLine(); line != null; line = buffer.readLine()){
				String[] data = line.split(", ");
				String name = data[0].trim();
				Double s1 = Double.parseDouble(data[1].trim());
				Double s2 = Double.parseDouble(data[2].trim());
				double sum = s1+s2;
				
				Homework hw = new Homework(name);
				hw.setScore(sum);
				elist.add(hw);
				
			}
			
			System.out.println("------- Homework -------");
			System.out.println(" ");
			System.out.println("Name       Average");
			System.out.println("-----  -------------");
			
			for(Homework h:hlist){
				System.out.println(h.getName()+"        "+h.getScore()/5.00);				
			}
			
			System.out.println("------------------------");
			System.out.println(" ");
			System.out.println(" ");
			
			
			System.out.println("------- Exam Scores -------");
			System.out.println(" ");
			System.out.println("Name       Average");
			System.out.println("-----  -------------");
			
			for(Homework e:elist){
				System.out.println(e.getName()+"        "+e.getScore()/2.00);				
			}
			
			System.out.println("---------------------------");
			
			FileWriter fileWriter = null;
			fileWriter = new FileWriter("average.txt",true);
			BufferedWriter out = new BufferedWriter(fileWriter);

			for(Homework e:elist){
				out.write(e.getName()+" "+e.getScore()/2.00);
				out.newLine();
			}
			out.flush();
		
		}
		catch (FileNotFoundException e){
			System.err.println("Cannot read file "+filename);
		}
		catch (IOException e){
			System.err.println("Error reading from file");
		}
		finally {
			try {
				if (fileReader != null)
						fileReader.close();
			} catch (IOException e) {
				System.err.println("Error closing files");
			}
	}
	
}


}
